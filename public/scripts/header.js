window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementById("buttonBar").style.background = "linear-gradient(#ECF0F1,#5D6D7E)";
        document.getElementById("logoBar").style.fontSize = "medium";
    } else {
        document.getElementById("buttonBar").style.background = "linear-gradient(#ECF0F1,#5D6D7E)";
        document.getElementById("logoBar").style.fontSize = "x-large";
    }
}