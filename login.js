let Sqlite = require('better-sqlite3');
exports.db = new Sqlite('db.sqlite',{ verbose: console.log });
exports.initialize = function(){
    let users_id = this.list();
    let stmt_income = this.db.prepare('insert into income(salary, others) values (?,?)')
    let stmt_expenses = this.db.prepare('insert into expenses(rent,charges, others) values (?,?,?)')
    let insert = this.db.transaction((users)=>{
        this.db.prepare('DELETE FROM income').run();
        this.db.prepare('DELETE FROM expenses').run();
        this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'income\';').run();
        this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'expenses\';').run();
        for(let user in users){
            stmt_income.run(0,0);
            stmt_expenses.run(0,0,0)
        }
    });
    insert(users_id);
    return users_id;
}
exports.refresh_income_and_expenses_users = function (){
    let users_id = this.list();
    let income = this.list();
    let expenses = this.list();
    let insert = this.db.transaction((length)=>{
            for(let i=0;i<length+1;i++){
                this.db.prepare('insert into income(salary, others) values (?,?)').run(0,0);
                this.db.prepare('insert into expenses(rent,charges, others) values (?,?,?)').run(0,0,0);
            }
    });
    insert(users_id.length-income.length);
    console.log("income's and expenses' database updated");
}
exports.update = function (id,salary,other_income,rent,charges,other_expenses){
    this.db.prepare('update income set salary=? where id=?').run(salary,id);
    this.db.prepare('update income set others=? where id=?').run(other_income,id);
    this.db.prepare('update expenses set rent=? where id=?').run(rent,id);
    this.db.prepare('update expenses set charges=? where id=?').run(charges,id);
    this.db.prepare('update expenses set others=? where id=?').run(other_expenses,id);
}
exports.list = function() {
    this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'users\';');
    return this.db.prepare('SELECT * FROM users ORDER BY id;').all();
};
exports.listIncome = function() {
    this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'income\';');
    return this.db.prepare('SELECT * FROM income ORDER BY id;').all();
};
exports.listExpenses = function() {
    this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'expenses\';');
    return this.db.prepare('SELECT * FROM expenses ORDER BY id;').all();
};
exports.listUsernames = function() {
    this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'users\';');
    return this.db.prepare('SELECT id,username FROM users ORDER BY id;').all();
};
exports.listPasswords = function() {
    this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'users\';');
    return this.db.prepare('SELECT id,password FROM users ORDER BY id;').all();
};
exports.read = function (id){
    let users = this.list();
    if(id in users) {
        this.db.prepare('UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME=\'users\';').run();
        return users[id];
    } else {
        return null;
    }
}
exports.create = function (username,email,password){
        let user = {
            username:username,
            email:email,
            password:password
        }
        this.db.prepare('INSERT INTO users(username, email, password) VALUES (?,?,?)').run(username,email,password);
        return user.id;
}

exports.findUser = (username,password) =>{
    let users = this.list();
    for(let i=0;i<users.length;i++){
        if(username===users[i].username && password===users[i].password){
            return users[i];
        }
    }
}
exports.updatePie = (id)=>{
    return this.listIncome()[id];
}
exports.updatePie1 = (id)=>{
    return this.listExpenses()[id];
}