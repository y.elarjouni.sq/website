let express = require('express');
let mustache = require('mustache-express');
const session = require('express-session');
const cookieParser = require('cookie-parser')

const login = require('./login');
let app = express();

app.engine('html',mustache());
app.use(cookieParser());
app.use(express.urlencoded({extended: true}));
app.use(express.static('./public'));
app.use(express.json());
app.use(session({
    name : "userId",
    secret : 'mot-de-passe-du-cookie',
    resave : false,
    saveUninitialized : false,
    cookie : {
        maxAge : 1000*60*5
    }
}));
app.set('view engine', 'html');
app.set('views', './views');

const redirectDashboard = (req, res, next) => {
    if (req.session.userId) {
        res.redirect("/dashboard");
    } else {
        next();
    }
};
const redirectLogin = (req, res, next) => {
    if (!req.session.userId) {
        res.redirect("/login-form");
    } else {
        next();
    }
};
app.get('/',(req,res)=>{
    const {userId} = req.session;
    res.render('Homepage',{userId});
});
app.get("/dashboard", redirectLogin,(req, res) => {
    let username = login.read(req.session.userId-1).username;
    exports.income = login.listIncome()[req.session.userId-1];
    exports.expenses = login.listExpenses()[req.session.userId-1];
    let {userId} = req.session;
    res.render('dashboard',{username,userId});
});
app.get('/login-form',redirectDashboard,(req,res)=>{
    res.render('login-form');
});
app.get("/login",redirectDashboard,((req, res) =>{
    res.redirect('/dashboard');
}));
app.get('/Register-form',redirectDashboard,(req,res)=>{
    res.render('Register-form');
});
app.get('/Register',redirectDashboard,(req,res)=>{
    res.redirect('/dashboard');
});
app.get('/about-us',(req,res)=>{
    const {userId} = req.session;
    res.render('AboutUs',{userId});
});
app.get('/help',(req,res)=>{
    const {userId} = req.session;
    res.render('Help',{userId});
});
app.get('/income-expenses',redirectLogin,(req,res)=>{
    res.render('IncomeExpenses');
});
app.get('/saving',redirectLogin,(req,res)=>{
    let remainingAmount = req.query.remainingAmount;
    const showRemainingAmount = remainingAmount>0;
    res.render('Saving',{remainingAmount,showRemainingAmount});
});
app.post("/login",redirectDashboard,((req, res) =>{
    const {username,password} = req.body;
    if(username && password){
        const user = login.findUser(username,password);
        if(user){
            req.session.userId = user.id;
            return res.redirect('/dashboard')
        }
    }
    res.redirect('/login-form');
}));
app.post('/register',redirectDashboard,(req, res) => {
    const {username,email,password} = req.body;
    let users = login.list();
    if(username && email && password){
        let exists = users.some(
            user => users.email === email
        );

        if(!exists){
            let newUser = {
                username:username,
                email:email,
                password:password
            }
            login.create(username,email,password);
            login.refresh_income_and_expenses_users();
            let user = login.findUser(newUser.username,newUser.password);
            req.session.userId = user.id;
            return res.redirect('/dashboard');
        }
    }
    res.redirect('/register-form')
});
app.post('/logout',redirectLogin,(req,res)=>{
    req.session.destroy(err =>{
        if(err){
            return res.redirect('/dashboard')
        }
        res.clearCookie("userId")
        res.redirect("/")
    })
});
app.post('/income-expenses',(req,res)=>{
    let {salary,other_income,rent,charges,other_expenses} = req.body;
    login.update(req.session.userId,salary,other_income,rent,charges,other_expenses);
    let incomes=[parseInt(salary),parseInt(other_income)];
    let income=0;
    for(let i = 0; i < incomes.length; i++){
        if(isNaN(incomes[i])){
            incomes[i]=0;
        }
        else{
            income+=incomes[i];
        }
    }
    let expenses = [parseInt(rent),parseInt(charges),parseInt(other_expenses)];
    let expense=0;
    for(let i = 0; i < expenses.length; i++){
        if(isNaN(expenses[i])){
            expenses[i]=0;
        }
        else{
            expense+=expenses[i];
        }
    }
    console.log(incomes,expenses);
    console.log(income,expense);
    console.log(parseInt(income)-parseInt(expenses));
    let remainingAmount = parseInt(income)-parseInt(expenses);
    console.log(remainingAmount);
    res.redirect(`/saving?remainingAmount=${remainingAmount}`);
});

app.listen(3000, () => console.log('server on at http://localhost:3000'));